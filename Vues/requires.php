<link rel="manifest" href="../manifest.json">
<link rel="apple-touch-icon" sizes="512x512" href="../Photos/Icons/icon_512x512.png">
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
<meta name="theme-color" content="white" media="(prefers-color-scheme: light)">
<meta name="theme-color" content="black" media="(prefers-color-scheme: dark)">


<script>
    if (navigator.serviceWorker != null) {
        navigator.serviceWorker.register('sw.js')
            .then(function(registration) {
                console.log('Registered events at scope: ', registration.scope);
            });
    }
</script>


<?php
class MyDB extends SQLite3{
    function __construct()
    {
        $this->open('../projet.db');
    }
}
/*
require '../vendor/autoload.php';

require_once('../CAS/CAS.php');


// Enable verbose error messages. Disable in production!
phpCAS::setVerbose(true);

// Initialise la connexion CAS
phpCAS::client(
    CAS_VERSION_2_0, "casserver.herokuapp.com", 443, 'cas', true,null,null
);

// Authentification de l'utilisateur
phpCAS::forceAuthentication();

// Récupération des informations de l'utilisateur
$user = phpCAS::getUser();
$attributes = phpCAS::getAttributes();

// Affichage des informations de l'utilisateur
echo "Nom d'utilisateur : $user<br>";
echo "Nom complet : " . $attributes['cn'] . "<br>";
echo "Email : " . $attributes['mail'] . "<br>";
*/
$db = new MyDB();
$db->enableExceptions();
if(!$db){
    echo $db->lastErrorMsg();
} else {
    //echo "Opened database successfully<br><br>";
}
session_start();
//On démarre une nouvelle session

header("Cache-Control:no-cache");

//On définit des variables de session
$_SESSION['prenom'] = 'Hugo';
$_SESSION['nom'] = 'DENIZOT';
$_SESSION['id'] = 2;


$maxSize = 1000000;

date_default_timezone_set('Europe/Paris')
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Poste BDE Sigma</title>
    <link rel="shortcut icon" href="../Photos/bdesigma2k22.png" />
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../CSS/style.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW"
            crossorigin="anonymous"></script>
</head>
<body>

<nav class="navbar navbar-dark" style="background-color: #397D32">
    <div class="container-fluid">
        <a class="navbar-brand" href="#"><img src="../Photos/bdesigma2k22.png" width="100" height="50"/> </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="Accueil.php">
                        Accueil
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="ajoutPoste.php">
                        Ajouter Poste
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="profil.php">
                        Afficher profil
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="ModifAdmin.php">
                        Modifier Administrateur
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="GestionFiltres.php ">
                        Filtres
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="GestionPostes.php ">
                        Gestions des postes par clubs
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>