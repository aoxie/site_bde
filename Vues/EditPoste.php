<?php
require_once 'requires.php';


if(isset($_GET['id'])){
    $sql='SELECT id_club FROM Poste WHERE id_poste='.$_GET['id'];
    $row=$db->query($sql)->fetchArray(SQLITE3_ASSOC);
    if($row!=null){
        $sql='SELECT count(*) FROM Administrateur WHERE club_id='.$row['id_club'].' and user_id='.$_SESSION['id'].';';
        $row=$db->query($sql)->fetchArray(SQLITE3_ASSOC);
    }
    if($row['count(*)']!=0){
        if(isset($_POST['titre'])&&isset($_POST['contenu'])){

            $sql = "UPDATE Poste "
                . "SET nom = :nom, "
                . "texte = :texte, "
                . "time = :time "
                . "WHERE id_poste = :id_poste";

            $stmt = $db->prepare($sql);

            $filtered_contenu = htmlspecialchars($_POST['contenu'], ENT_QUOTES, 'UTF-8');
            $filtered_titre = htmlspecialchars($_POST['titre'], ENT_QUOTES, 'UTF-8');
            // passing values to the parameters
            $stmt->bindValue(':nom', $filtered_titre);
            $stmt->bindValue(':texte',$filtered_contenu);
            $stmt->bindValue(':time', time());
            $stmt->bindValue(':id_poste', $_GET['id']);

            // execute the update statement
            $stmt->execute();
            header("Location: Accueil.php", true, 301);

        }

        $sql='SELECT * from Poste Where id_poste='.$_GET['id'];
        $row=$db->query($sql)->fetchArray(SQLITE3_ASSOC);


        $sql='SELECT * FROM Poste WHERE id_poste='.$_GET['id'];
        $ret=$db->query($sql);
        $row = $ret->fetchArray(SQLITE3_ASSOC);

        echo '
    <hr>
    <div style="text-align: center">';

        if(file_exists("../Photos/Postes/".$row['id_poste'].".jpg")){
            echo '<img src="../Photos/Postes/'.$row['id_poste'].'.jpg" width="200" height="200"/>';
        }else{
            echo "<div>Le poste n'a pas encore de photo</div>";
        }

        echo'
       <form action="EditPoste.php?id='.$_GET['id'].'" method="post">
        </br>
        <label>Titre du poste: </label>
        <input type="text" name="titre" value="'.$row['nom'].'">
        </br>
        <label>Contenu du poste: </label>
        <textarea type="text" name="contenu" style="overflow-scrolling: auto" cols="25" rows="10">'.$row['texte'].'</textarea>
        </br>
        
        <button type="submit">Modifier le poste</button>
        </form>
        <form enctype="multipart/form-data" method="POST" action="EditPoste.php?id='.$_GET['id'].'">
           <input name="myfile" type="file" /></br>
           <input type="submit" name="submit" />
        </form>
        <div>La photo doit être en <B>jpg</B></div>
    </div>
    <hr>';

        if (isset($_POST['submit'])) {
            $validExt = array('.jpg');

            if ($_FILES['myfile']['error'] > 0) {
                echo "Une erreur est survenue lors du transfert";
                die;
            }

            $fileSize = $_FILES['myfile']['size'];

            if ($fileSize > $maxSize) {
                echo "Le fichier est trop gros !";
                die;
            }

            $fileName = $_FILES['myfile']['name'];
            $fileExt = "." . strtolower(substr(strrchr($fileName, '.'), 1));

            if (!in_array($fileExt, $validExt)) {
                echo "Le fichier n'est pas en format jpg !";
                die;
            }

            $tmpName = $_FILES['myfile']['tmp_name'];
            $fileName = "../Photos/Postes/" . $row['id_poste'] . $fileExt;
            $resultat = move_uploaded_file($tmpName, $fileName);

            if ($resultat) {
                header("Location: Accueil.php", true, 301);
            }
        }

    }
    else{
        header("Location: Accueil.php", true, 301);
    }
}else{
    header("Location: Accueil.php", true, 301);
}
?>