<?php

require_once 'requires.php';


if(isset($_POST['titre'])&&isset($_POST['contenu'])&&isset($_POST['id_club'])){
    //$insert='INSERT INTO Poste VALUES(null,"'.$_POST['titre'].'",(Select id From Users where nom="'.$_SESSION['nom'].'" AND prenom="'.$_SESSION['prenom'].'"),"'.$_POST['contenu'].'",'.time().','.$_POST['id_club'].');';

    $sql = "INSERT INTO Poste "
        . "VALUES(null, "
        . ":titre, "
        . ":id, "
        . ":contenu,"
        . ":times,"
        . ":id_club)";

    $stmt = $db->prepare($sql);

    // passing values to the parameters
    $filtered_contenu = htmlspecialchars($_POST['contenu'], ENT_QUOTES, 'UTF-8');
    $filtered_titre = htmlspecialchars($_POST['titre'], ENT_QUOTES, 'UTF-8');
    $stmt->bindValue(':titre', $filtered_titre);
    $stmt->bindValue(':id', $_SESSION['id']);
    $stmt->bindValue(':contenu', $filtered_contenu);
    $stmt->bindValue(':times', time());
    $stmt->bindValue(':id_club', $_POST['id_club']);

    // execute the update statement
    $stmt->execute();

    //$db->query($insert);
    header("Location: Accueil.php", true, 301);
}
$reqclub='SELECT * FROM Club WHERE id_club in (SELECT club_id FROM Administrateur where user_id='.$_SESSION['id'].')';
$club = $db->query($reqclub);
echo '
    <hr>
    <div style="text-align: center">
       <form action="ajoutPoste.php" method="post">
       <label>Sélectionner le club: </label>
       <select name="id_club">';

        while($row = $club->fetchArray(SQLITE3_ASSOC)){
            echo '<option value="'.$row['id_club'].'" label="'.$row['nom'].'">'.$row['nom'].'</option>';
        }
echo'    </select>
        </br>
        <label>Titre du poste: </label>
        <input type="text" name="titre"/>
        </br>
        <label>Contenu du poste: </label>
        <textarea type="text" name="contenu" style="overflow-scrolling: auto" cols="25" rows="10"></textarea>
        </br>
        <button type="submit">Ajouter Poste</button>
    </div>
    </form>
    </div>
    <hr>';


if (isset($_POST['submit'])) {
    $validExt = array('.jpg', '.jpeg', '.gif', '.png');

    if ($_FILES['myfile']['error'] > 0) {
        echo "Une erreur est survenue lors du transfert";
        die;
    }

    $fileSize = $_FILES['myfile']['size'];

    if ($fileSize > $maxSize) {
        echo "Le fichier est trop gros !";
        die;
    }

    $fileName = $_FILES['myfile']['name'];
    $fileExt = "." . strtolower(substr(strrchr($fileName, '.'), 1));

    if (!in_array($fileExt, $validExt)) {
        echo "Le fichier n'est pas une image !";
        die;
    }

    $tmpName = $_FILES['myfile']['tmp_name'];
    $fileName = "../Photos/Poste/" . $_SESSION['id'] . $fileExt;
    $resultat = move_uploaded_file($tmpName, $fileName);

    if ($resultat) {
        echo "Transfert terminé";
    }
}

