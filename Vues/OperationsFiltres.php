<?php
header('Location: '."Accueil.php");

class MyDB extends SQLite3{
    function __construct()
    {
        $this->open('../projet.db');
    }
}

$db = new MyDB();
if(!$db){
    echo $db->lastErrorMsg();
} else {
    echo "Opened database successfully<br><br>";
}

session_start();

//On définit des variables de session
$_SESSION['prenom'] = 'Hugo';
$_SESSION['nom'] = 'DENIZOT';

$delete='DELETE FROM Filtres WHERE id_user=(SELECT id FROM Users Where nom="'.$_SESSION['nom'].'" AND prenom="'.$_SESSION['prenom'].'");';
$db->query($delete);

if(isset($_POST['Filtres']))
{
    foreach($_POST['Filtres'] as $valeur)
    {
        $insert='INSERT INTO Filtres VALUES((SELECT id FROM Users WHERE nom="'.$_SESSION['nom'].'" AND prenom="'.$_SESSION['prenom'].'"),(SELECT id_club FROM Club WHERE nom="'.$valeur.'"));';
        $db->query($insert);
    }
}
Exit();