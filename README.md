---
id: SIGMA_BDE_SITE_Version-1.0
author: Ao XIE, Hugo DENIZOT
authorURL: aoxie.art
title: Sigma Student Union Website Project
---
<div align="center">
    <img src = "./Photos/bdesigma2k22.png">
</div>

# Projet de site web du BDE Sigma Clermont

![php version](https://img.shields.io/badge/PHP-%3E%3D7.0-orang)
![SQLite3](https://img.shields.io/badge/SQLite-3-orange)

[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

Ce projet est un site web d'information pour les utilisateurs. L'utilisateur peut consulter toutes les informations en se connectant avec un nom d'utilisateur et un mot de passe.
## Contexte du projet
Le projet est né de la nécessité pour le bureau des étudiants d'adapter le site web afin qu'il puisse être utilisé pour diffuser des informations aux étudiants. Chaque élève peut se connecter avec son propre nom d'utilisateur et son propre mot de passe. Une fois connectés, les étudiants peuvent voir toutes les notifications et ils peuvent également sélectionner le contenu pertinent en choisissant différents classificateurs.
## Mainteneurs
@hudenizot
@aoxie
## Points forts du projet
Les points forts suivants existent dans la réalisation du site web.
- Connectez avec le compte Sigma et le mot de passe.
- Ouverte à tous les utilisateurs.
- Une section enverra des nouvelles.
- Nouvelles avec filtrage.
- Module distinct de gestion des droits.
## Flux de projets

