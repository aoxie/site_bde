<?php

    require_once("AdminGateway.php");
    require_once("Connection.php");
    require_once("Session.php");

class Connexion extends PDO
{
    private $stmt;

    function __construct($d, $u, $p){
        parent::__construct($d, $u, $p);
        $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    function executeQuery(string $query, array $parameter=[]) :bool{
        $this->stmt =parent::prepare($query);
        foreach($parameter as $name => $value){
            $this->stmt->bindValue($name, $value[0], $value[1]);
        }
        return $this->stmt->execute();
    }

    public function getResults():array{
        return $this->stmt->fetchAll();
    }
}