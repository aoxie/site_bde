<?php


class MdlLogin
{
    private $ag;
    function __construct(Connection $con) {
        $this->ag = new AdminGateway($con);
    }

    function connexion($login, $password) {
        $passwordFromDataBase = $this->ag->getPassword($login);
        if(password_verify($password, $passwordFromDataBase[0][0])) {
            $_SESSION['pseudo'] = $login;
        } else {
            $message= "Login ou mdp incorrect";
            require_once ("../vues/erreur.php");
        }
    }

    function deconnexion() {
        $_SESSION = array();
        session_unset();
        session_destroy();
    }

    function isAdmin() {
        if(isset($_SESSION['login'])) {
            return true;
        } else {
            return false;
        }
    }
}