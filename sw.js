// 缓存名称
const CACHE_NAME = 'my-cache-v1';

// 缓存所需的文件
const urlsToCache = [
    '/Vues/Accueil.php',
    '/Vues/requires.php',
    '/Photos/profil.jpg'
];

// 当 Service Worker 安装时缓存所需的文件
self.addEventListener('install', function(event) {
    event.waitUntil(
        caches.open(CACHE_NAME)
            .then(function(cache) {
                console.log('Opened cache');
                return cache.addAll(urlsToCache);
            })
    );
});

// 当用户请求资源时，尝试从缓存中返回它们
self.addEventListener('fetch', function(event) {
    event.respondWith(
        caches.match(event.request)
            .then(function(response) {
                if (response) {
                    return response;
                }

                return fetch(event.request);
            })
    );
});

// 在 Service Worker 激活后删除旧的缓存
self.addEventListener('activate', function(event) {
    event.waitUntil(
        caches.keys().then(function(cacheNames) {
            return Promise.all(
                cacheNames.map(function(cacheName) {
                    if (cacheName !== CACHE_NAME) {
                        return caches.delete(cacheName);
                    }
                })
            );
        })
    );
});